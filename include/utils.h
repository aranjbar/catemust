//
// Created by kali on 2/26/20.
//

#ifndef _UTILS_H
#define _UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <arpa/inet.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

void uuid_hex(char*, int);
void get_printable_address(char*, struct in_addr, in_port_t);
void print_error(char*);
void print_status(char*);
void print_message(char*);
void print_good(char*);
void print_prefix(char);
unsigned int len(const char*);
char* itoa_simple(char*, int);

#endif //_UTILS_H
