//
// Created by kali on 2/26/20.
//

#ifndef _TYPES_H
#define _TYPES_H

#include <sys/socket.h>

/* constants */
#define SID_LEN               32
#define NAME_LEN              32
#define SERVER_SOCKET_BACKLOG 13
#define ARRAY_DEFAULT_SIZE    10
#define BUFFER_DEFAULT_SIZE   8192
#define MAX_PACKET_SIZE       SID_LEN + NAME_LEN + BUFFER_DEFAULT_SIZE + 3
#define BROADCAST_IP          "127.255.255.255"
#define ROOM_ACTIVE_TIME      30
#define SIG_ALARM_TIME        5

/* events */
#define CREATE     "create"
#define USERNAME   "username"
#define MESSAGE    "message"
#define JOIN       "join"
#define GET        "get"
#define EXIT       "exit"
#define SIG_UPDATE "sig_update"
#define CHAT       "chat"

/* error codes */
#define ERR_SOCKET_CREATE   1
#define ERR_SOCKET_BIND     2
#define ERR_SOCKET_LISTEN   3
#define ERR_SOCKET_CONNECT  4
#define ERR_PACKET_UNKNOWN  5
#define ERR_COMMAND_START   6
#define ERR_COMMAND_UNKNOWN 7

/* basics */
typedef int error_t;
typedef unsigned short int uint16_t;
typedef char sid_t[SID_LEN + 1];
typedef int socket_t;

/*
 * enums
 */
typedef enum bool {
    true  = 1,
    false = 0,
} bool;

typedef enum packet_type {
    CONNECT    = 0,
    DISCONNECT = 1,
    EVENT      = 2,
    INPUT    = 3,
    UNKNOWN    = 4,
} packet_type;

/**
 * structs
 */
typedef struct pool_t pool_t;
typedef struct packet_t packet_t;
typedef struct room_t room_t;

struct pool_t {
    socket_t max_fd;
    fd_set read_fds;
    fd_set write_fds;
    int num_ready;
};

struct packet_t {
    packet_type type;
    sid_t sid;
    char name[NAME_LEN + 1];
    char data[BUFFER_DEFAULT_SIZE + 1];

    void (*init)(packet_t*, packet_type, char*, char*, char*);
    void (*make_raw_data)(packet_t*, char*);
};

struct room_t {
    char name[NAME_LEN + 1];
    socket_t socket;
    struct sockaddr_in* address;
    uint16_t port_number;
    bool public;
    time_t last_update;
};


/* declarations */
packet_t* new_packet(void);

#endif //_TYPES_H
