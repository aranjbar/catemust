//
// Created by kali on 2/28/20.
//

#ifndef _SERVER_H
#define _SERVER_H

#include "types.h"

/**
 * structs
 */
typedef struct server_t server_t;
typedef struct manager_t manager_t;
typedef struct client_t client_t;

manager_t* new_manager(server_t*);

struct server_t {
    socket_t socket;
    struct sockaddr_in *address;
    pool_t* pool;
    client_t** clients;
    uint num_clients;
    uint max_clients;
    manager_t* manager;

    error_t (*run)(server_t*, uint16_t);
    void (*_renew_pool)(server_t*);
    packet_t* (*_get_packet)(server_t*);
    error_t (*_send_packet)(server_t*, char*, packet_t*);
    client_t* (*_find_client_by_sid)(server_t*, char*);
    client_t* (*_find_client_by_name)(server_t*, char*);
    socket_t (*_get_client_socket_by_sid)(server_t*, char*);
    void (*_add_client)(server_t* this, socket_t, char*);
    void (*_delete_client_by_index)(server_t*, uint);
    void (*create_room)(server_t*, char*, uint16_t, bool);
    room_t* (*enter_room)(server_t*, char*, char*);
    void (*get_rooms_name_raw)(server_t*, char*);
    void (*check_rooms_activity)(server_t*);
    void (*update_room_by_port_number)(server_t*, uint16_t);
    void (*_generate_id)(char*);
    u_int16_t (*_random_port)();
    void (*_handle_connect)(server_t*);
    void (*_handle_disconnect)(server_t*);
    void (*_handle_event)(server_t*, packet_t*);
    void (*_handle_command)(server_t*, packet_t*);
    void (*clear_up)(server_t*);
};

struct manager_t {
    server_t* server;
    room_t** rooms;
    uint num_rooms;
    uint max_rooms;

    room_t* (*add_room)(manager_t*, char*, uint16_t, bool);
    void (*remove_room_by_index)(manager_t*, uint);
//    void (*connect)(manager_t*, char*, uint16_t, bool);
    room_t* (*enter_room)(manager_t*, char*, char*);
    room_t* (*_find_room_by_name)(manager_t*, char*);
    void (*remove_inactive_rooms)(manager_t*);
    void (*update_room_by_port_number)(manager_t*, uint16_t);
};

struct client_t {
    sid_t sid;
    socket_t socket;
    room_t* room;
    char username[NAME_LEN + 1];
};

#endif //_SERVER_H
