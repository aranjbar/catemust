//
// Created by kali on 2/28/20.
//

#ifndef _CLIENT_H
#define _CLIENT_H

#include "types.h"

/**
 * structs
 */
typedef struct client_t client_t;

struct client_t {
    sid_t sid;
    char username[NAME_LEN + 1];
    socket_t comm_socket;
    room_t* room;
//    socket_t mess_socket;
    pool_t* pool;

    error_t (*connect)(client_t*, uint16_t);
    error_t (*run)(client_t*);
    void (*_renew_pool)(client_t*);
    packet_t* (*_get_packet)(client_t*);
    error_t (*_send_packet)(client_t*, packet_t*);
    void (*update_activity)(client_t*);
    void (*_handle_connect)(client_t*, packet_t*);
    void (*_handle_disconnect)(client_t*, packet_t*);
    void (*_handle_event)(client_t*, packet_t*);
    void (*_handle_input)(client_t*, packet_t*);
    void (*_close_room)(client_t*);
    void (*check_error)(error_t, bool*);
};

#endif //_CLIENT_H
