//
// Created by kali on 3/3/20.
//

#include "utils.h"

void uuid_hex(char* uuid, int len) {
    unsigned int i = 0, char_idx = 0;
    char hex_char[17] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    srandom(time(NULL));
    for (i = 0; i < len; ++i) {
        char_idx = random()%16;
        uuid[i] = hex_char[char_idx];
    }
    uuid[len] = '\0';
}

unsigned int len(const char* str) {
    int i = 0;
    for (i = 0; str[i] != '\0'; ++i) {}
    return i;
}

static char *itoa_simple_helper(char *dest, int i) {
    if (i <= -10) {
        dest = itoa_simple_helper(dest, i/10);
    }
    *dest++ = '0' - i%10;
    return dest;
}

char *itoa_simple(char *dest, int i) {
    char *s = dest;
    if (i < 0) {
        *s++ = '-';
    } else {
        i = -i;
    }
    *itoa_simple_helper(s, i) = '\0';
    return dest;
}

//void itoa(char* str, unsigned int i) {
//    unsigned int digit;
//    char a[1] = {'\0'};
//    while (i > 0) {
//        digit = i%10;
//        (*a) = (char)(digit + '0');
//        strcat(str, a);
//        i /= 10;
//    }
//}

void get_printable_address(char* addr, struct in_addr sin_addr, in_port_t sin_port) {
    char data[10];
    strcat(addr, inet_ntoa(sin_addr));
    strcat(addr, ":");
    itoa_simple(data, sin_port);
    strcat(addr, data);
}

void print_error(char* message) {
    write(STDERR_FILENO, "\033[0`", 4);
    write(STDERR_FILENO, KRED, 5);
    write(STDERR_FILENO, "[-] ", 4);
    write(STDERR_FILENO, KNRM, 4);
    write(STDERR_FILENO, message, len(message));
    write(STDERR_FILENO, "\n", 1);
}

void print_status(char* message) {
    write(STDOUT_FILENO, "\033[0`", 4);
    write(STDOUT_FILENO, KBLU, 5);
    write(STDOUT_FILENO, "[*] ", 4);
    write(STDOUT_FILENO, KNRM, 4);
    write(STDOUT_FILENO, message, len(message));
    write(STDOUT_FILENO, "\n", 1);
}

void print_message(char* message) {
    write(STDOUT_FILENO, "\033[0`", 4);
    write(STDOUT_FILENO, KBLU, 5);
    write(STDOUT_FILENO, "[>] ", 4);
    write(STDOUT_FILENO, KNRM, 4);
    write(STDOUT_FILENO, message, len(message));
    write(STDOUT_FILENO, "\n", 1);
}

void print_good(char* message) {
    write(STDOUT_FILENO, "\033[0`", 4);
    write(STDOUT_FILENO, KGRN, 5);
    write(STDOUT_FILENO, "[+] ", 4);
    write(STDOUT_FILENO, KNRM, 4);
    write(STDOUT_FILENO, message, len(message));
    write(STDOUT_FILENO, "\n", 1);
}

void print_prefix(char prefix) {
    write(STDOUT_FILENO, "\033[0`", 4);
    write(STDOUT_FILENO, KBLU, 5);
    write(STDOUT_FILENO, "[", 1);
    write(STDOUT_FILENO, &prefix, 1);
    write(STDOUT_FILENO, "] ", 2);
    write(STDOUT_FILENO, KNRM, 4);
}
