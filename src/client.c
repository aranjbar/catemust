//
// Created by kali on 2/26/20.
//

#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>

#include "client.h"
#include "utils.h"

client_t* new_client();
error_t client_t_connect(client_t*, uint16_t);
error_t client_t_run(client_t*);
void client_t_renew_pool(client_t*);
packet_t* client_t_get_packet(client_t*);
error_t client_t_send_packet(client_t*, packet_t*);
void client_t_update_activity(client_t*);
void client_t_handle_connect(client_t*, packet_t*);
void client_t_handle_disconnect(client_t*, packet_t*);
void client_t_handle_event(client_t*, packet_t*);
void client_t_handle_input(client_t *this, packet_t *pkt);
void client_t_close_room(client_t*);
void client_t_check_error(error_t, bool*);
void signal_handler(int);

volatile sig_atomic_t alarm_flag = false;

int main(int argc, char *argv[]) {
    error_t err;
    if (argc < 2) {
        print_error("Please provide server port number.");
        exit(EXIT_FAILURE);
    }
    uint16_t port_number = (uint16_t)strtol(argv[1], NULL, 10);


    client_t* client = new_client();
    err = client->connect(client, port_number);
    if (err != 0) {
        exit(EXIT_FAILURE);
    }
    print_status("Connected.");
    err = client->run(client);
    if (err != 0) {
        exit(EXIT_FAILURE);
    }
}

client_t* new_client() {
    client_t* c = (client_t*)malloc(sizeof(client_t));
    c->comm_socket = -1;
    c->room = NULL;
    c->pool = (pool_t*)malloc(sizeof(pool_t));
    c->pool->max_fd = 2;
//    FD_ZERO(&(c->pool->read_fds));
//    FD_ZERO(&(c->pool->write_fds));
//    FD_SET(STDIN_FILENO, &(c->pool->read_fds));


    c->connect = client_t_connect;
    c->run = client_t_run;
    c->_renew_pool = client_t_renew_pool;
    c->_get_packet = client_t_get_packet;
    c->_send_packet = client_t_send_packet;
    c->update_activity = client_t_update_activity;
    c->_handle_connect = client_t_handle_connect;
    c->_handle_disconnect = client_t_handle_disconnect;
    c->_handle_event = client_t_handle_event;
    c->_handle_input = client_t_handle_input;
    c->_close_room = client_t_close_room;
    c->check_error = client_t_check_error;
    return c;
}

error_t client_t_connect(client_t* this, uint16_t port_number) {
    int err;
    int client_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (client_socket < 0) {
        print_error("Error creating socket.");
        return ERR_SOCKET_CREATE;
    }

    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port_number);
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    err = connect(client_socket, (struct sockaddr *) &server_addr, sizeof(server_addr));
    if (err < 0) {
        print_error("Server is not online.\n");
        return ERR_SOCKET_CONNECT;
    }

    this->comm_socket = client_socket;

    return 0;
}

error_t client_t_run(client_t* this) {
    bool run = true;
    error_t err = 0;
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 0;

    print_prefix('#');
    while (run) {
        this->_renew_pool(this);
        if (alarm_flag) {
            this->update_activity(this);
            alarm_flag = false;
            alarm(SIG_ALARM_TIME);
        }
        this->pool->num_ready = select(this->pool->max_fd + 1,
                                       &(this->pool->read_fds),
                                       &(this->pool->write_fds),
                                       NULL,
                                       &tv);

        if (this->pool->num_ready < 1) {
            continue;
        }

        packet_t* pkt = this->_get_packet(this);

        switch (pkt->type) {
            case CONNECT:
                this->_handle_connect(this, pkt);
                break;
            case DISCONNECT:
                this->_handle_disconnect(this, pkt);
                break;
            case EVENT:
                this->_handle_event(this, pkt);
                break;
            case INPUT:
                this->_handle_input(this, pkt);
                break;
            default:
                err = ERR_PACKET_UNKNOWN;
        }

        print_prefix('#');
        free(pkt);
        this->check_error(err, &run);
    }

    return err;
}

void client_t_renew_pool(client_t* this) {
    FD_ZERO(&(this->pool->read_fds));
    FD_ZERO(&(this->pool->write_fds));
    FD_SET(STDIN_FILENO, &(this->pool->read_fds));
    if (this->comm_socket != -1) {
        FD_SET(this->comm_socket, &(this->pool->read_fds));
        if (this->comm_socket > this->pool->max_fd) {
            this->pool->max_fd = this->comm_socket;
        }
    }
    if (this->room == NULL) {
        return;
    }
    if (this->room->socket != -1) {
        FD_SET(this->room->socket, &(this->pool->read_fds));
        if (this->room->socket > this->pool->max_fd) {
            this->pool->max_fd = this->room->socket;
        }
    }
}

packet_t* client_t_get_packet(client_t* this) {
    char* pch = NULL;
    char data[MAX_PACKET_SIZE] = {'\0'};
    size_t num_byte_read = 0;
    packet_t* pkt = new_packet();

    if (FD_ISSET(this->comm_socket, &(this->pool->read_fds))) {
        num_byte_read = read(this->comm_socket , data, MAX_PACKET_SIZE);
        print_status(data);
        if (num_byte_read == 0) {
            pkt->type = DISCONNECT;
        } else {
            pch = strtok(data,";\n");
            if (pch != NULL) {
                strncpy(pkt->sid, pch, SID_LEN);
                pkt->sid[SID_LEN] = '\0';
            }

            pch = strtok(NULL, ";\n");
            if (pch != NULL) {
                strcpy(pkt->name, pch);
                if (strcmp(pkt->name, "connect") == 0) {
                    pkt->type = CONNECT;
                } else {
                    pkt->type = EVENT;
                }
            }

            pch = strtok(NULL, ";\n");
            if (pch != NULL) {
                strcpy(pkt->data, pch);
            }
        }
    } else if (FD_ISSET(STDIN_FILENO, &(this->pool->read_fds))) {
        pkt->type = INPUT;
        read(STDIN_FILENO, pkt->data, BUFFER_DEFAULT_SIZE);
    } else if (FD_ISSET(this->room->socket, &(this->pool->read_fds))) {
        num_byte_read = recvfrom(this->room->socket, data, MAX_PACKET_SIZE, 0, NULL, 0);
        if (num_byte_read < 0) {
            print_error("Error getting message");
        }

        if (data[0] == '\0') {
            socket_t new_socket;
            int server_address_len = sizeof(*(this->room->address));

            // accept connection
            new_socket = accept(this->room->socket,
                                       (struct sockaddr*) (this->room->address),
                                       (socklen_t*) &server_address_len);
            if (new_socket < 0) {
                print_error("Error accepting connection request.");
                this->_close_room(this);
            } else {
                close(this->room->socket);
                this->room->socket = new_socket;

                FD_SET(this->room->socket, &(this->pool->read_fds));
                if (this->room->socket > this->pool->max_fd) {
                    this->pool->max_fd = this->room->socket;
                }
            }
        } else {
            pch = strtok(data,";\n");
            if (pch != NULL) {
                strncpy(pkt->sid, pch, SID_LEN);
                pkt->sid[SID_LEN] = '\0';
            }

            pch = strtok(NULL, ";\n");
            if (pch != NULL) {
                strcpy(pkt->name, pch);
            }

            pkt->type = EVENT;

            pch = strtok(NULL, ";\n");
            if (pch != NULL) {
                strcpy(pkt->data, pch);
            }
        }

    } else {
        pkt->type = UNKNOWN;
    }

    return pkt;
}

error_t client_t_send_packet(client_t* this, packet_t* pkt) {
    size_t num_byte_write = -1;
    char data[MAX_PACKET_SIZE] = {'\0'};
    pkt->make_raw_data(pkt, data);
    num_byte_write = write(this->comm_socket, data, len(data));
    return num_byte_write;
}

void client_t_update_activity(client_t* this) {
    error_t err;
    if (this->room == NULL) {
        return;
    }
    if (this->comm_socket == -1) {
        return;
    }
    if (this->room->public) {
        packet_t* packet = new_packet();
        char data[6] = {'\0'};
        itoa_simple(data, this->room->port_number);
        packet->init(packet, EVENT, this->sid, SIG_UPDATE, data);

        err = this->_send_packet(this, packet);
        if (err < 0) {
            print_error("Error sending packet");
        }
        free(packet);
    }
}
void client_t_handle_connect(client_t* this, packet_t* pkt) {
    strncpy(this->sid, pkt->sid, SID_LEN);
    this->sid[SID_LEN] = '\0';
}

void client_t_handle_disconnect(client_t* this, packet_t* pkt) {

}

void client_t_handle_event(client_t* this, packet_t* pkt) {
    error_t err;
    if (strncmp(pkt->name, CREATE, 6) == 0) {

    } else if (strncmp(pkt->name, JOIN, 4) == 0) {
        uint16_t port_number = (uint16_t)strtol(pkt->data, NULL, 10);
        socket_t sock;
        int opt = 1;

        sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (sock < 0) {
            print_error("Error creating socket");
        }

        err = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*)&opt, sizeof(opt));
        if (err < 0) {
            print_error("Error set socket option (SO_REUSEADDR)");
        }
        this->room = (room_t*)malloc(sizeof(room_t));
        this->room->address = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));

        memset(this->room->address, '\0', sizeof(*(this->room->address)));
        this->room->address->sin_family = AF_INET;
        this->room->address->sin_addr.s_addr = htonl(INADDR_ANY);
        this->room->address->sin_port = htons(port_number);

        err = bind(sock, (struct sockaddr*)this->room->address, sizeof(*(this->room->address)));
        if (err < 0) {
            print_error("Error joining group.");
            return;
        }

        this->room->name[0] = '\0';
        this->room->socket = sock;
        this->room->port_number = port_number;
        this->room->public = true;

        FD_SET(this->room->socket, &(this->pool->read_fds));
        if (this->room->socket > this->pool->max_fd) {
            this->pool->max_fd = this->room->socket;
        }

        signal(SIGALRM, signal_handler);
        alarm(SIG_ALARM_TIME);
    } else if (strncmp(pkt->name, MESSAGE, 7) == 0) {
        char* pch;
        char message[BUFFER_DEFAULT_SIZE] = {'\0'};
        pch = strtok(pkt->data, ":");
        if (strcmp(this->username, pch) != 0) {
            strcat(message, pch);
            pch = strtok(NULL, ":");
            strcat(message, ":");
            strcat(message, pch);
            print_message(message);
        }
    } else if (strncmp(pkt->name, GET, 3) == 0) {
        char* pch;
        pch = strtok(pkt->data, ",");
        print_status("Available groups:");
        while (pch != NULL) {
            print_good(pch);
            pch = strtok(NULL, ",");
        }
    } else if (strncmp(pkt->name, CHAT, 4) == 0) {
        if (strncmp(pkt->data, "busy", 4) == 0) {
            print_status("Client is in another chat.");
        } else {
            if (this->room != NULL) {
                packet_t* packet = new_packet();
                packet->init(packet, EVENT, this->sid, EXIT, "");

                err = this->_send_packet(this, packet);
                if (err < 0) {
                    print_error("Error sending packet");
                }
                free(packet);

                this->_close_room(this);
            }
            char* pch;
            char* pch1;
            pch1 = strtok(pkt->data, ",\n");
            pch = strtok(NULL, ",\n");
            uint16_t port_number = (uint16_t)strtol(pch, NULL, 10);
            socket_t sock;
            int opt = 1;

            sock = socket(AF_INET, SOCK_STREAM, 0);
            if (sock < 0) {
                print_error("Error creating socket");
            }

            err = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*)&opt, sizeof(opt));
            if (err < 0) {
                print_error("Error set socket option (SO_REUSEADDR)");
            }

            this->room = (room_t*)malloc(sizeof(room_t));
            this->room->address = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));

            memset(this->room->address, '\0', sizeof(*(this->room->address)));
            this->room->address->sin_family = AF_INET;
            this->room->address->sin_addr.s_addr = htonl(INADDR_ANY);
            this->room->address->sin_port = htons(port_number);

            if (strncmp(pch1, "server", 6) == 0) {
                err = bind(sock, (struct sockaddr*)this->room->address, sizeof(*(this->room->address)));
                if (err < 0) {
                    print_error("Error joining group.");
                    return;
                }

                err = listen(sock, SERVER_SOCKET_BACKLOG);
                if (err < 0) {
                    print_error("Error listening socket.");
                    return;
                }
            } else if (strncmp(pch1, "client", 6) == 0) {
                err = connect(sock, (struct sockaddr*)this->room->address, sizeof(*(this->room->address)));
                if (err < 0) {
                    print_error("Server is not online.\n");
                    return;
                }

            }
            this->room->name[0] = '\0';
            this->room->socket = sock;
            this->room->port_number = port_number;
            this->room->public = false;

            this->room->socket = sock;

            FD_SET(this->room->socket, &(this->pool->read_fds));
            if (this->room->socket > this->pool->max_fd) {
                this->pool->max_fd = this->room->socket;
            }
        }


    }
}

void client_t_handle_input(client_t* this, packet_t* pkt) {
    char* pch;
    error_t err;
    if (pkt->data[0] == '/') {
        pch = strtok(pkt->data, "/\n ");
        if (strncmp(pch, "username", 8) == 0) {
            pch = strtok(NULL, "/\n ");

            packet_t* packet = new_packet();
            packet->init(packet, EVENT, this->sid, USERNAME, pch);
            strcpy(this->username, pch);

            err = this->_send_packet(this, packet);
            if (err < 0) {
                print_error("Error sending packet");
            }
            free(packet);
        } else if (strncmp(pch, CREATE, 6) == 0) {
            pch = strtok(NULL, "/\n ");

            packet_t* packet = new_packet();
            packet->init(packet, EVENT, this->sid, CREATE, pch);

            err = this->_send_packet(this, packet);
            if (err < 0) {
                print_error("Error sending packet");
            }
            free(packet);
        } else if (strncmp(pch, JOIN, 4) == 0) {
            pch = strtok(NULL, "/\n ");

            packet_t* packet = new_packet();
            packet->init(packet, EVENT, this->sid, JOIN, pch);

            err = this->_send_packet(this, packet);
            if (err < 0) {
                print_error("Error sending packet");
            }
            free(packet);
        } else if (strncmp(pch, GET, 3) == 0) {
            pch = strtok(NULL, "/\n ");

            packet_t* packet = new_packet();
            packet->init(packet, EVENT, this->sid, GET, pch);

            err = this->_send_packet(this, packet);
            if (err < 0) {
                print_error("Error sending packet");
            }
            free(packet);
        } else if (strncmp(pch, EXIT, 4) == 0) {
            pch = strtok(NULL, "/\n ");

            packet_t* packet = new_packet();
            packet->init(packet, EVENT, this->sid, EXIT, pch);

            err = this->_send_packet(this, packet);
            if (err < 0) {
                print_error("Error sending packet");
            }
            free(packet);

            this->_close_room(this);
        } else if (strncmp(pch, CHAT, 4) == 0) {
            pch = strtok(NULL, "/\n ");

            packet_t* packet = new_packet();
            packet->init(packet, EVENT, this->sid, CHAT, pch);

            err = this->_send_packet(this, packet);
            if (err < 0) {
                print_error("Error sending packet");
            }
            free(packet);
        }
    } else {
        if (this->room == NULL) {
            print_error("You aren't in any group or private chat.");
            return;
        } else if (this->room->public) {
            char data[MAX_PACKET_SIZE] = {'\0'};
            packet_t* packet = new_packet();
            strcat(data, this->username);
            strcat(data, ": ");
            strcat(data, pkt->data);
            packet->init(packet, EVENT, this->sid, MESSAGE, data);
            err = this->_send_packet(this, packet);
            if (err < 0) {
                print_error("Error sending message.");
            }
            free(packet);
        } else {
            char data[MAX_PACKET_SIZE] = {'\0'};
            packet_t* packet = new_packet();
            // data[0] = '\0';
            strcat(data, this->username);
            strcat(data, ": ");
            strcat(data, pkt->data);
            packet->init(packet, EVENT, this->sid, MESSAGE, data);

            char raw[MAX_PACKET_SIZE] = {'\0'};
            size_t num_byte_write = -1;
            packet->make_raw_data(packet, raw);
            num_byte_write = write(this->room->socket, raw, len(raw));

            if (num_byte_write < 0) {
                print_error("Error sending message.");
            }
            free(packet);
        }
    }
}

void client_t_close_room(client_t* this) {
    if (this->room == NULL) {
        return;
    }
    free(this->room->address);
    close(this->room->socket);
    free(this->room);
    this->room = NULL;
}

void client_t_check_error(error_t err, bool* flag) {

}

void signal_handler(int param) {
    alarm_flag = true;
}