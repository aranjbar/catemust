//
// Created by kali on 2/26/20.
//

#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include "server.h"
#include "utils.h"

server_t* new_server();
client_t* new_client();
error_t server_t_run(server_t*, uint16_t);
void server_t_renew_pool(server_t*);
packet_t* server_t_get_packet(server_t*);
error_t server_t_send_packet(server_t*, char*, packet_t*);
client_t* server_t_find_client_by_sid(server_t*, char*);
client_t* server_t_find_client_by_name(server_t*, char*);
socket_t server_t_get_client_socket_by_sid(server_t*, char*);
void server_t_add_client(server_t*, socket_t, char*);
void server_t_delete_client_by_index(server_t*, uint);
void server_t_create_room(server_t*, char*, uint16_t, bool);
room_t* server_t_enter_room(server_t*, char*, char*);
void server_t_get_rooms_name_raw(server_t*, char*);
void server_t_check_rooms_activity(server_t*);
void server_t_update_room_by_port_number(server_t*, uint16_t);
void server_t_generate_id(char*);
uint16_t server_t_random_port();
void server_t_handle_connect(server_t*);
void server_t_handle_disconnect(server_t*);
void server_t_handle_event(server_t*, packet_t*);
void server_t_handle_command(server_t*, packet_t*);
void server_t_clear_up(server_t*);

int main(int argc, char* argv[]) {
    if (argc < 2) {
        print_error("Please provide server port number.");
        exit(EXIT_FAILURE);
    }
    uint16_t port_number = (uint16_t)strtol(argv[1], NULL, 10);

    server_t* server = new_server();
    error_t err = server->run(server, port_number);
    if (err != 0) {
        exit(EXIT_FAILURE);
    }

    return 0;
}

client_t* new_client() {
    client_t* c = (client_t*)malloc(sizeof(client_t));
    c->room = NULL;
    c->sid[0] = '\0';
    c->username[0] = '\0';
    c->socket = -1;

    return c;
}

server_t* new_server() {
    server_t* s = (server_t*)malloc(sizeof(server_t));
    s->address = (struct sockaddr_in*) malloc(sizeof(struct sockaddr_in));
    s->pool = (pool_t*)malloc(sizeof(pool_t));
    s->pool->max_fd = 2;

    s->clients = (client_t**)malloc(ARRAY_DEFAULT_SIZE*sizeof(client_t*));
    s->num_clients = 0;
    s->max_clients = ARRAY_DEFAULT_SIZE;

    s->manager = new_manager(s);

    s->run = server_t_run;
    s->_renew_pool = server_t_renew_pool;
    s->_get_packet = server_t_get_packet;
    s->_send_packet = server_t_send_packet;
    s->_find_client_by_sid = server_t_find_client_by_sid;
    s->_find_client_by_name = server_t_find_client_by_name;
    s->_get_client_socket_by_sid = server_t_get_client_socket_by_sid;
    s->_add_client = server_t_add_client;
    s->_delete_client_by_index = server_t_delete_client_by_index;
    s->create_room = server_t_create_room;
    s->enter_room = server_t_enter_room;
    s->get_rooms_name_raw = server_t_get_rooms_name_raw;
    s->check_rooms_activity = server_t_check_rooms_activity;
    s->update_room_by_port_number = server_t_update_room_by_port_number;
    s->_generate_id = server_t_generate_id;
    s->_random_port = server_t_random_port;
    s->_handle_connect = server_t_handle_connect;
    s->_handle_disconnect = server_t_handle_disconnect;
    s->_handle_event = server_t_handle_event;
    s->_handle_command = server_t_handle_command;
    s->clear_up = server_t_clear_up;

    return s;
}

error_t server_t_run(server_t* this, uint16_t port_number) {
    error_t err;

    this->socket = socket(AF_INET, SOCK_STREAM, 0);
    if (this->socket < 0) {
        print_error("Error creating socket.");
        return ERR_SOCKET_CREATE;
    }

    memset(this->address, '\0', sizeof(*(this->address)));
    this->address->sin_family = AF_INET;
    this->address->sin_port = htons(port_number);
    this->address->sin_addr.s_addr = htonl(INADDR_ANY);

    err = bind(this->socket, (struct sockaddr*) (this->address), sizeof(*(this->address)));
    if (err < 0) {
        print_error("Error binding socket.");
        return ERR_SOCKET_BIND;
    }

    err = listen(this->socket, SERVER_SOCKET_BACKLOG);
    if (err < 0) {
        print_error("Error listening socket.");
        return ERR_SOCKET_LISTEN;
    }

    print_status("Waiting for connections ...");
    print_prefix('#');
    while (true) {
        this->check_rooms_activity(this);
        this->_renew_pool(this);
        this->pool->num_ready = select(this->pool->max_fd + 1,
                                       &(this->pool->read_fds),
                                       &(this->pool->write_fds),
                                       NULL,
                                       NULL);

        if (this->pool->num_ready < 0 && errno != EINTR) {
            print_error("Error select.");
        }

        if (this->pool->num_ready == 0) {
            continue;
        }

        packet_t* pkt = this->_get_packet(this);

        if (pkt->type == CONNECT) {
            this->_handle_connect(this);
        } else if (pkt->type == DISCONNECT) {
            this->_handle_disconnect(this);
        } else if (pkt->type == EVENT) {
            this->_handle_event(this, pkt);
        } else if (pkt->type == INPUT) {
            this->_handle_command(this, pkt);
        } else {
            return ERR_PACKET_UNKNOWN;
        }

        print_prefix('#');
        free(pkt);
    }
}

void server_t_renew_pool(server_t* this) {
    FD_ZERO(&(this->pool->read_fds));
    FD_ZERO(&(this->pool->write_fds));

    FD_SET(STDIN_FILENO, &(this->pool->read_fds));
    FD_SET(this->socket, &(this->pool->read_fds));
    if (this->socket > this->pool->max_fd) {
        this->pool->max_fd = this->socket;
    }

    int i = 0;
    for (i = 0; i < this->num_clients; ++i) {
        client_t* c = this->clients[i];
        FD_SET(c->socket, &(this->pool->read_fds));
        if (c->socket > this->pool->max_fd) {
            this->pool->max_fd = this->socket;
        }
    }
}

packet_t* server_t_get_packet(server_t* this) {
    uint i = 0, pos = 0;
    size_t num_byte_read = 0;
    char * pch;
    char data[MAX_PACKET_SIZE] = {'\0'};
    packet_t* pkt = new_packet();

    if (FD_ISSET(this->socket, &(this->pool->read_fds))) {
        pkt->type = CONNECT;
    } else if (FD_ISSET(STDIN_FILENO, &(this->pool->read_fds))) {
        pkt->type = INPUT;
        read(STDIN_FILENO , pkt->data, BUFFER_DEFAULT_SIZE);
    } else {
        for (i = 0; i < this->num_clients; ++i) {
            client_t* c = this->clients[i];
            if (FD_ISSET(c->socket, &(this->pool->read_fds))) {
                num_byte_read = read(c->socket , data, MAX_PACKET_SIZE);
                print_status(data);

                if (num_byte_read == 0) {
                    pkt->type = DISCONNECT;
                } else {
                    pch=strchr(data,';');
                    pos = (uint)(pch - data);
                    if (pos == SID_LEN) {
                        pkt->type = EVENT;
                        pch = strtok(data, ";\n");
                        if (pch != NULL) {
                            strncpy(pkt->sid, pch, SID_LEN);
                            pkt->sid[SID_LEN] = '\0';
                        }

                        pch = strtok(NULL, ";\n");
                        if (pch != NULL) {
                            strcpy(pkt->name, pch);
                        }

                        pch = strtok(NULL, ";\n");
                        if (pch != NULL) {
                            strcpy(pkt->data, pch);
                        }
                    } else {
                        pkt->type = UNKNOWN;
                    }
                }
                break;
            }
        }
    }
    return pkt;
}

error_t server_t_send_packet(server_t* this, char* sid, packet_t* pkt) {
    error_t err;
    char data[MAX_PACKET_SIZE] = {'\0'};
    socket_t sock = this->_get_client_socket_by_sid(this, sid);
    pkt->make_raw_data(pkt, data);
    err = write(sock, data, len(data));
    return err;
}

client_t* server_t_find_client_by_sid(server_t* this, char* sid) {
    uint i = 0;
    client_t* client = NULL;
    for (i = 0; i < this->num_clients; ++i) {
        client_t* c = this->clients[i];
        if (strncmp(c->sid, sid, SID_LEN) == 0) {
            client = c;
            break;
        }
    }
    return client;
}

client_t* server_t_find_client_by_name(server_t* this, char* username) {
    uint i = 0;
    client_t* client = NULL;
    for (i = 0; i < this->num_clients; ++i) {
        client_t* c = this->clients[i];
        if (strcmp(c->username, username) == 0) {
            client = c;
            break;
        }
    }
    return client;
}

socket_t server_t_get_client_socket_by_sid(server_t* this, char* sid) {
    client_t* c = this->_find_client_by_sid(this, sid);
    return c->socket;
}

void server_t_add_client(server_t* this, socket_t socket, char* sid) {
    // Create new client
    client_t* c = new_client();
    c->socket = socket;
    strncpy(c->sid, sid, SID_LEN + 1);

    // Add new client to server clients list
    if (this->num_clients == this->max_clients) {
        this->clients = (client_t**)realloc(this->clients, (ARRAY_DEFAULT_SIZE + this->max_clients)*sizeof(client_t*));
    }
    this->clients[this->num_clients] = c;
    this->num_clients = this->num_clients + 1;

    FD_SET(socket, &(this->pool->read_fds));
    if (socket > this->pool->max_fd) {
        this->pool->max_fd = socket;
    }
}

void server_t_delete_client_by_index(server_t* this, uint idx) {
    // TODO: free client.
    uint i = 0;
    for (i = idx; i < this->num_clients - 1; ++i) {
        this->clients[i] = this->clients[i + 1];
    }
    this->clients[this->num_clients - 1] = NULL;
    this->num_clients = this->num_clients - 1;
}

void server_t_create_room(server_t* this, char* _name, uint16_t _port_number, bool _public) {
    this->manager->add_room(this->manager, _name, _port_number, _public);
    // TODO: Check if room created already
}

room_t* server_t_enter_room(server_t* this, char* name, char* sid) {
    return this->manager->enter_room(this->manager, sid, name);
}

void server_t_get_rooms_name_raw(server_t* this, char* data) {
    uint i = 0;
    for (i = 0; i < this->manager->num_rooms; ++i) {
        room_t* r = this->manager->rooms[i];
        strcat(data, r->name);
        if (i != this->manager->num_rooms - 1) {
            strcat(data, ",");
        }
    }
}

void server_t_check_rooms_activity(server_t* this) {
    this->manager->remove_inactive_rooms(this->manager);
}

void server_t_update_room_by_port_number(server_t* this, uint16_t port_number) {
    this->manager->update_room_by_port_number(this->manager, port_number);
}

void server_t_generate_id(char* id) {
    uuid_hex(id, SID_LEN);
}

uint16_t server_t_random_port() {
    uint16_t port_number = 0;
    struct sockaddr_in address;
    socket_t sock;
    error_t err = -1;
    srandom(time(NULL));

    while (err < 0) {
        port_number = random()%10000 + 53000;

        sock = socket(AF_INET, SOCK_STREAM, 0);
        if (sock < -1) {
            print_error("Error creating socket.");
        }

        memset(&address, '\0', sizeof(address));
        address.sin_family = AF_INET;
        address.sin_port = htons(port_number);
        address.sin_addr.s_addr = inet_addr(BROADCAST_IP);

        err = bind(sock, (struct sockaddr*)&address, sizeof(address));
        if (err < 0) {
            print_error("Error binding socket.");
        }
        close(sock);
    }

    return port_number;
}

/*
 * Handle a client connection request.
 */
void server_t_handle_connect(server_t* this) {
    char sid[SID_LEN + 1] = {'\0'}, str[129]={'\0'}, printable_address[23]={'\0'};
    socket_t new_client_socket;
    int server_address_len = sizeof(*(this->address));

    // accept connection
    new_client_socket = accept(this->socket,
                               (struct sockaddr*) (this->address),
                               (socklen_t*) &server_address_len);
    if (new_client_socket < 0) {
        print_error("Error accepting connection request.");
        exit(EXIT_FAILURE);
    }

    this->_generate_id(sid);

    // log new connection
    get_printable_address(printable_address, this->address->sin_addr, this->address->sin_port);
    strcat(str, "New connection from ");
    strcat(str, printable_address);
    strcat(str, ", sid=");
    strcat(str, sid);
    print_good(str);

    this->_add_client(this, new_client_socket, sid);

    // Send connect replay. (containing sid)
    packet_t* packet = new_packet();
    packet->init(packet, CONNECT, sid, "connect", sid);
    this->_send_packet(this, sid, packet);
//    this->manager->enter_room(sid, sid);
}

void server_t_handle_disconnect(server_t* this) {
    uint i = 0;
    char message[129] = {'\0'};
    for (i = 0; i < this->num_clients; ++i) {
        client_t* c = this->clients[i];
        if (FD_ISSET(c->socket, &(this->pool->read_fds))) {
            strcat(message, "Disconnected: ");
            strcat(message, c->sid);
            print_status(message);
            this->_delete_client_by_index(this, i);
        }
    }
}

void server_t_handle_event(server_t* this, packet_t* pkt) {
    if (strncmp(pkt->name, USERNAME, 8) == 0) {
        client_t* c = this->_find_client_by_sid(this, pkt->sid);
        if (c == NULL) {
            return;
        }
        strncpy(c->username, pkt->data, NAME_LEN);
        c->username[NAME_LEN] = '\0';
    } else if (strncmp(pkt->name, CREATE, 6) == 0) {
        char data[6] = {'\0'};
        uint16_t port_number = this->_random_port();
        itoa_simple(data, port_number);
        this->create_room(this, pkt->data, port_number, true);
    } else if (strncmp(pkt->name, JOIN, 4) == 0) {
        packet_t* packet = new_packet();
        char data[6] = {'\0'};
        room_t* r = this->enter_room(this, pkt->data, pkt->sid);
        itoa_simple(data, r->port_number);
        packet->init(packet, EVENT, pkt->sid, JOIN, data);
        this->_send_packet(this, packet->sid , packet);
        free(packet);
    } else if (strncmp(pkt->name, MESSAGE, 7) == 0) {
        client_t* c = this->_find_client_by_sid(this, pkt->sid);
        uint send_string_len = 0;
        if (c == NULL) {
            return;
        }
        if (c->room == NULL) {
            return;
        }

        if (c->room->public) {
            char data[MAX_PACKET_SIZE] = {'\0'};
            pkt->make_raw_data(pkt, data);
            send_string_len = sendto(c->room->socket, data, len(data), 0,
                                     (struct sockaddr*)c->room->address, sizeof(*(c->room->address)));
            if (send_string_len != len(data)) {
                print_error("Error: Sent a different number of bytes than expected.");
            }
        }
    } else if (strncmp(pkt->name, GET, 3) == 0) {
        packet_t* packet = new_packet();
        char data[BUFFER_DEFAULT_SIZE] = {'\0'};
        this->get_rooms_name_raw(this, data);
        packet->init(packet, EVENT, pkt->sid, GET, data);
        this->_send_packet(this, packet->sid, packet);
        free(packet);
    } else if (strncmp(pkt->name, EXIT, 4) == 0) {
        client_t* c = this->_find_client_by_sid(this, pkt->sid);
        c->room = NULL;
    } else if (strncmp(pkt->name, SIG_UPDATE, 10) == 0) {
        uint16_t port_number = (uint16_t)strtol(pkt->data, NULL, 10);
        this->update_room_by_port_number(this, port_number);
    } else if (strncmp(pkt->name, CHAT, 4) == 0) {
        client_t* c = this->_find_client_by_name(this, pkt->data);
        if (c == NULL) {
            return;
        }
        if (c->room == NULL) {
            char port[6] = {'\0'};
            uint16_t port_number = this->_random_port();
            itoa_simple(port, port_number);

            char data[13] = {'\0'};
            strcat(data, "server,");
            strcat(data, port);
            packet_t* packet = new_packet();
            packet->init(packet, EVENT, pkt->sid, CHAT, data);

            this->_send_packet(this, packet->sid, packet);
            free(packet);

            data[0] = '\0';
            strcat(data, "client,");
            strcat(data, port);
            packet = new_packet();
            packet->init(packet, EVENT, c->sid, CHAT, data);

            this->_send_packet(this, c->sid, packet);
            free(packet);
        } else {
            packet_t* packet = new_packet();
            packet->init(packet, EVENT, pkt->sid, CHAT, "busy");

            this->_send_packet(this, packet->sid, packet);
            free(packet);
        }
    }
}

void server_t_handle_command(server_t* this, packet_t* pkt) {
    char* pch;
    char message[129] = {'\0'};
    if (pkt->data[0] != '/') {
        print_error("Commands start with / character.");
        return;
    }

    pch = strtok(pkt->data,"/\n");
    if (strncmp(pch, "shutdown", 8) == 0) {
        this->clear_up(this);
        exit(EXIT_SUCCESS);
    } else {
        strcat(message, "Command not found: ");
        strcat(message, pch);
        print_error(message);
    }
}

void server_t_clear_up(server_t* this) {
    int i = 0;
    close(this->socket);
    free(this->address);
    free(this->pool);
    free(this->manager);
    for (i = 0; i < this->num_clients; ++i) {
        free(this->clients[i]);
    }
}