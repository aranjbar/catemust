//
// Created by kali on 2/29/20.
//

#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "types.h"
#include "utils.h"

void packet_t_init(packet_t*, packet_type, char*, char*, char*);
void packet_t_make_raw_data(packet_t*, char*);

packet_t* new_packet(void) {
    packet_t* p= (packet_t*)malloc(sizeof(packet_t));
    p->sid[0] = '\0';
    p->name[0] = '\0';
    p->data[0] = '\0';

    p->init = packet_t_init;
    p->make_raw_data = packet_t_make_raw_data;
    return p;
}

void packet_t_init(packet_t* this, packet_type _type, char* _sid, char* _name, char* _data) {
    this->type = _type;
    this->sid[0] = '\0';
    this->name[0] = '\0';
    this->data[0] = '\0';
    strcpy(this->sid, _sid);
    strcpy(this->name, _name);
    if (_data != NULL) {
        strcpy(this->data, _data);
    }

}

void packet_t_make_raw_data(packet_t* this, char* data) {
    strncpy(data, this->sid, SID_LEN);
    strcat(data, ";");
    strcat(data, this->name);
    strcat(data, ";");
    strcat(data, this->data);
}
