//
// Created by kali on 2/26/20.
//

#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#include "server.h"
#include "utils.h"

room_t* manager_t_add_room(manager_t*, char*, uint16_t, bool);
void manager_t_remove_room_by_index(manager_t*, uint);
//void manager_t_connect(manager_t*, char*, uint16_t , bool);
room_t* manager_t_enter_room(manager_t*, char*, char*);
room_t* manager_t_find_room_by_name(manager_t*, char*);
void manager_t_remove_inactive_rooms(manager_t*);
void manager_t_update_room_by_port_number(manager_t*, uint16_t);

manager_t* new_manager(server_t* _server) {
    manager_t* m = malloc(sizeof(manager_t));
    m->rooms = (room_t**)malloc(ARRAY_DEFAULT_SIZE*sizeof(room_t*));
    m->num_rooms = 0;
    m->max_rooms = ARRAY_DEFAULT_SIZE;
    m->server = _server;

    m->add_room = manager_t_add_room;
    m->remove_room_by_index = manager_t_remove_room_by_index;
//    m->connect = manager_t_connect;
    m->enter_room = manager_t_enter_room;
    m->_find_room_by_name = manager_t_find_room_by_name;
    m->remove_inactive_rooms = manager_t_remove_inactive_rooms;
    m->update_room_by_port_number = manager_t_update_room_by_port_number;
    return m;
}

room_t* new_room(char* _name, uint16_t _port_number, bool _public) {
    room_t* r = (room_t*)malloc(sizeof(room_t));
    r->name[0] = '\0';
    strncpy(r->name, _name, NAME_LEN);
    r->name[NAME_LEN] = '\0';
    r->port_number = _port_number;
    r->public = _public;
    time(&(r->last_update));

    if (_public) {
        socket_t sock;
        int broadcast_permission;
        error_t err;
        struct sockaddr_in* address = (struct sockaddr_in*) malloc(sizeof(struct sockaddr_in));;
        sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (sock < 0) {
            print_error("Error creating socket");
        }

        broadcast_permission = 1;
        err = setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *)&broadcast_permission, sizeof(broadcast_permission));
        if (err < 0) {
            print_error("Error setting socket option");
        }

        memset(address, 0, sizeof(*address));
        address->sin_family = AF_INET;
        address->sin_addr.s_addr = inet_addr(BROADCAST_IP);
        address->sin_port = htons(_port_number);

        r->socket = sock;
        r->address = address;
    }

    return r;
}

room_t* manager_t_add_room(manager_t* this, char* _name, uint16_t _port_number, bool _public) {
    room_t* r = new_room(_name, _port_number, _public);

    if (this->num_rooms == this->max_rooms) {
        this->rooms = (room_t**)realloc(this->rooms, (ARRAY_DEFAULT_SIZE + this->max_rooms)*sizeof(room_t*));
    }
    this->rooms[this->num_rooms] = r;
    this->num_rooms = this->num_rooms + 1;

    return r;
}

//void manager_t_connect(manager_t* this, char* sid, uint16_t _port_number, bool public) {
//    this->enter_room(this, sid, sid, socket, public);
//}

void manager_t_remove_room_by_index(manager_t* this, uint idx) {
    free(this->rooms[idx]->address);
    close(this->rooms[idx]->socket);
    free(this->rooms[idx]);

    uint i = 0;
    for (i = idx; i < this->num_rooms - 1; ++i) {
        this->rooms[i] = this->rooms[i + 1];
    }
    this->rooms[this->num_rooms - 1] = NULL;
    this->num_rooms = this->num_rooms - 1;
}

room_t* manager_t_enter_room(manager_t* this, char* sid, char* name) {
    room_t* r = this->_find_room_by_name(this, name);
    if (r == NULL) {
        // room doesn't exist.
    }
    client_t* c = this->server->_find_client_by_sid(this->server, sid);
    if (c == NULL) {
        // client doesn't exist.
        return NULL;
    }
    c->room = r;
    return r;
}

room_t* manager_t_find_room_by_name(manager_t* this, char* name) {
    uint i = 0;
    for (i = 0; i < this->num_rooms; ++i) {
        room_t* r = this->rooms[i];
        if (strncmp(r->name, name, NAME_LEN) == 0) {
            return r;
        }
    }

    return NULL;
}

void manager_t_remove_inactive_rooms(manager_t* this) {
    uint i = 0;
    for (i = 0; i < this->num_rooms; ++i) {
        double seconds = 0;
        time_t now;
        time(&now);
        seconds = difftime(now, this->rooms[i]->last_update);
        if (seconds > ROOM_ACTIVE_TIME) {
            this->remove_room_by_index(this, i);
            // TODO: send exit event to room's members.
        }
    }
}

void manager_t_update_room_by_port_number(manager_t* this, uint16_t port_number) {
    uint i = 0;
    for (i = 0; i < this->num_rooms; ++i) {
        room_t* r = this->rooms[i];
        if (r->port_number == port_number) {
            time(&(r->last_update));
        }
    }
}